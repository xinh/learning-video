//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by lip phi on 10/2/17.
//  Copyright © 2017 lip phi. All rights reserved.
//

import Foundation

struct CalculatorBrain {
    
    private var accumulator: Double?
    private enum Operation {
        case contans(Double)
        case unaryOperation((Double) -> Double)
        case binaryOperation((Double, Double) -> Double)
        case equals
    }
    private var operations: Dictionary<String, Operation> = [
        "π" : Operation.contans(Double.pi),
        "e" : Operation.contans(M_E),
        "√" : Operation.unaryOperation(sqrt),
        "cos": Operation.unaryOperation(cos),
        "±" : Operation.unaryOperation({ -$0 }),
        "×" : Operation.binaryOperation({ $0 * $1 }),
        "÷" : Operation.binaryOperation({ $0 / $1 }),
        "−" : Operation.binaryOperation({ $0 - $1 }),
        "+" : Operation.binaryOperation({ $0 + $1 }),
        "=" : Operation.equals
    ]
    
    mutating func performOperation (_ symbol: String) {
//        switch symbol {
//            case "π":
//                //displayText.text = String(Double.pi)
//                accumulator = Double.pi
//            case "√":
//                //                    let operand = Double(displayText.text!)
//                //                    displayText.text = String(sqrt(operand!))
//                if let operand = accumulator {
//                    accumulator = sqrt(operand)
//
//                }
//            default:
//                break
//        }
        if let operation  = operations[symbol] {
            switch operation {
                case .contans(let value):
                    accumulator = value
                break
                case .unaryOperation(let function):
                    if accumulator != nil {
                        accumulator = function(accumulator!)
                    }
                break
                case .binaryOperation(let function):
                    print("click binary")
                    if accumulator != nil {
                        bpo = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                        accumulator = nil
                    }
                break
                case .equals:
                    performPendingBinaryOperation()
                break
            }
        }else{
            print("Empty Symbol")
        }
        
        
    }
    
    private mutating func performPendingBinaryOperation() {
        if bpo != nil && accumulator != nil {
            accumulator = bpo!.perform(with: accumulator!)
            bpo = nil
        }
    }
    
    private var bpo : PendingBinaryOperation?
    
    private struct PendingBinaryOperation {
        let function: (Double,Double) -> Double
        let firstOperand: Double
        func perform(with secondOperand: Double) -> Double {
            return function(firstOperand , secondOperand)
        }
    }
    
    mutating func setOperand (_ operand: Double) {
        accumulator = operand
    }
    
    var result: Double? {
        get {
            return accumulator
        }
    }
    
}
