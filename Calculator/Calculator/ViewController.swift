//
//  ViewController.swift
//  Calculator
//
//  Created by lip phi on 10/2/17.
//  Copyright © 2017 lip phi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var displayText: UILabel!
    @IBOutlet weak var numberLabelText: UIButton!
    var userInTheMiddleOfTyping = false
    @IBAction func clickBtnNumber(_ sender: UIButton) {
        let optionalNumberText = sender.currentTitle!

        if userInTheMiddleOfTyping {
            let textCurrentDisplay = displayText.text!
            displayText.text = textCurrentDisplay + optionalNumberText
        } else {
            displayText.text = optionalNumberText
            userInTheMiddleOfTyping = true
        }
        
    }
    
    var displayValue: Double {
        get {
            return Double(displayText.text!)!
        }
        set {
            displayText.text =  String(newValue)
        }
    }
    
    private var brain: CalculatorBrain = CalculatorBrain()
    @IBAction func performOperation(_ sender: UIButton) {
        
        if userInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userInTheMiddleOfTyping = false
        }
        
        if let matchOperation = sender.currentTitle {
            brain.performOperation(matchOperation)
        }
        if let result = brain.result {
            displayValue = result
        }
        
    }
    
}

